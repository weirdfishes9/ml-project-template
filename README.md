# Project title

A subtitle that describes your project, e.g., research question

## Motivation

**Problem:**
...

**Solution:**
...

**Project constraints:**

- ...
- ...

## Method and results

First, introduce and motivate your chosen method, and explain how it contributes to solving the research question/business problem.

Second, summarize your results concisely. Make use of subheaders where appropriate.

## Repository overview

    ├── LICENSE
    |
    ├── README.md              <- The top-level README.
    |                             for developers using this project.
    |
    ├── CHANGELOG.md           <- Log of project changes.
    |
    ├── data
    │   ├── external           <- Data from third party sources.
    │   ├── interim            <- Intermediate data that has been transformed.
    │   ├── processed          <- The final, canonical data sets for modeling.
    │   └── raw                <- The original, immutable data dump.
    │
    ├── models                 <- Trained and serialized models,
    |                             model predictions, or model summaries.
    │
    ├── notebooks              <- Jupyter notebooks.
    │   ├── drafts             <- Notebooks for experiments.
    │   └── reports            <- Notebooks for reports creation.
    │
    ├── references             <- Data dictionaries, manuals, and
    |                             all other explanatory materials.
    │
    ├── reports                <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures            <- Generated graphics and figures
    |                             to be used in reporting.
    |
    ├── src                    <- Source code for use in this project.
    │   ├── __init__.py        <- Makes src a Python module.
    │   │
    │   ├── data               <- Scripts to download or generate data.
    │   │   ├── __init__.py
    │   │   ├── make_datasets.py
    │   │   └── preprocessing.py
    │   │
    │   ├── features           <- Scripts to turn raw data into features
    |   |   |                     for modeling.
    │   │   ├── __init__.py
    │   │   ├── build_features.py
    │   │   └── select_features.py
    │   │
    │   ├── models             <- Scripts to train models and then use
    |   |   |                     trained models to make predictions.
    │   │   ├── __init__.py
    │   │   ├── evaluate.py
    │   │   ├── predict.py
    │   │   └── train.py
    |   |
    │   ├── utils              <- Auxiliary packages.
    │   │   ├── __init__.py
    │   │
    │   └── visualization      <- Scripts to create exploratory and
    |       |                     results oriented visualizations.
    │       ├── __init__.py
    │       └── visualize.py
    |
    ├── tests                  <- Pipeline unit tests.
    │   ├── test_build_features.py
    │   ├── test_make_dataset.py
    │   ├── test_preprocessing.py
    │   ├── test_select_features.py
    │   └── test_train.py
    |
    ├── .env.example           <- List of environment variables to be mentioned
    |                             in .env local file.
    |
    ├── dvc.yaml               <- File with pipeline stages description.
    |
    ├── params.yaml            <- Parameters for dvc.yaml.
    │
    ├── setup.py               <- makes project pip installable
    |                             (pip install -e .) so src can be imported.
    │
    └── tox.ini                <- tox file with settings for running tox.

--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small>

## Running instructions

Explain to potential users how to run/replicate your workflow. If necessary, touch upon the required input data, which secret credentials are required (and how to obtain them), which software tools are needed to run the workflow (including links to the installation instructions), and how to run the workflow.

## More resources

Point interested users to any related literature and/or documentation.

## About

Explain who has contributed to the repository. You can say it has been part of a class you've taken at Tilburg University.

## Source

Link: <https://tilburgsciencehub.com/building-blocks/store-and-document-your-data/document-data/readme-best-practices/>
